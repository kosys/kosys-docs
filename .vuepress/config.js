
const { path } = require('@vuepress/shared-utils');

module.exports = (ctx) => {

    var theme = '@vuepress/theme-default';
    var plugins = [
        '@kosys-docs/vuepress-plugin-search'
    ];
    var base = "/kosys-docs/";
    var dest = "public/";

    //印刷モードの場合
    if (ctx.options.theme === "@kosys-docs/vuepress-theme-print") {
        theme = '@kosys-docs/vuepress-theme-print';
        plugins.push("@kosys-docs/book-manifest-generator");
        base += "print/";
        dest = "public/print/";
    } else {
        theme = "@kosys-docs/vuepress-theme-local";
    }
    return {
        title: 'こうしす！ 資料集',
        description: 'こうしす！の資料集です',
        base: base,
        dest: dest,
        locales: {
            "/": {
                lang: "ja"
            }
        },
        plugins: plugins,
        theme: theme,
        themeConfig: {
            nav: [
                { text: 'Home', link: '/' },
                { text: '作業手順書', link: '/manuals/' },
                { text: '設定資料集', link: '/docs/' }
            ],
            sidebar: {
                '/manuals/': [
                    '/manuals/',
                    {
                        title: '参加者共通　',
                        collapsable: false,
                        children: [　
                            '/manuals/register',
                            '/manuals/cla',
                            '/manuals/weekly-meeting',
                        ]
                    },
                    {
                        title: 'アニメーター作業手順',
                        collapsable: true,
                        children: [　
                            '/manuals/animation/',
                            '/manuals/animation/assign',
                            '/manuals/animation/upload',
                        ]
                    },
                    {
                        title: '撮影作業手順',
                        collapsable: true,
                        children: [
                            '/manuals/compositing/',
                            '/manuals/compositing/assign',
                            '/manuals/compositing/normalize-layers',
                            '/manuals/compositing/composite-kari',
                            '/manuals/compositing/composite-hon',
                            '/manuals/compositing/fix-problem',
                            '/manuals/compositing/design-note',
                            
                        ]
                    },
                    {
                        title: 'コンテ・演出作業手順',
                        collapsable: true,
                        children: [
                        ]
                    },
                    {
                        title: '監督作業手順',
                        collapsable: true,
                        children: [
                        ]
                    }
                ],
                '/docs/': [
                    '',
                    {
                        title: 'キャラクター設定',
                        collapsable: false,
                        children: [
                            'characters/',
                            'characters/akane-hosono'
                        ]
                    },
                    {
                        title: '世界設定',
                        collapsable: false,
                        children: [
                            'world/',
                            'world/timeline'
                        ]
                    },
                    {
                        title: '小物類',
                        collapsable: false,
                        children: [
                        ]
                    },
                ],
                '/': [

                ]
            }
        },        
        bookConfig: {
            author: "OPAP-JP contributors",
            bookUrl: "https://kosys.gitlab.io/kosys-docs/print/",
            cover: "cover.png",
            coverFormat: "image/png"
        },
        postcss: {
            plugins: []
        },
        chainWebpack: config => {
            // 最適化処理はVivliostyleと相性が悪いので削除する
            config.plugins.delete('optimize-css')
        },
    };
};