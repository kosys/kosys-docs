"use strict";

const { fs, path } = require("@vuepress/shared-utils");
const CopyPlugin = require("copy-webpack-plugin");

module.exports = (options, ctx) => ({
    chainWebpack (config, isServer)  {
        if (!isServer){ return; }

        var publicPrintDir = path.join(ctx.vuepressDir, "public.print");
        
        if (fs.existsSync(publicPrintDir)) {

            config
                .plugin("copy")
                .use(
                    CopyPlugin, 
                    [[
                        { from: publicPrintDir , to: ctx.outDir }
                    ]]
                );
        }
    
    },
    async generated (pagePaths) {
        const { generateBookManifest } = require("./manifestGenerator.js")(ctx);
        generateBookManifest();
    }
});

