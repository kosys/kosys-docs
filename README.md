---
home: true
---

## ライセンスについて

この冊子は、特に記載がない限り、クリエイティブ・コモンズ表示4.0国際（CC-BY 4.0)ライセンスのもとに無償で頒布されています。

![CC-BY 4.0](https://licensebuttons.net/l/by/4.0/88x31.png)

ライセンスの概要及び本文は以下からご覧いただけます。

<https://creativecommons.org/licenses/by/4.0/deed.ja>