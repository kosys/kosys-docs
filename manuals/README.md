# こうしす！作業手順書

## 内容

この資料は、アニメ「こうしす！」の作業手順を説明するものです。


## ライセンスについて

この資料は、特に記載がない限り、クリエイティブ・コモンズ表示4.0国際（CC-BY 4.0)ライセンスのもとに無償で頒布されています。

![CC-BY 4.0](https://licensebuttons.net/l/by/4.0/88x31.png)

ライセンスの概要及び本文は以下からご覧いただけます。

<https://creativecommons.org/licenses/by/4.0/deed.ja>

なお、この資料に含まれるスクリーンショットなどは上記のライセンスの対象とはなりません。

## クレジット

* OPAP-JP contributors (<https://opap.jp/contributors>)
