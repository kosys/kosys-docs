# 作画作業の流れ


こうしす！は少人数体制での制作のため、一般的な商業作品に比べて簡易なワークフローを採用しています。

こうしす！では、以下の流れで作画を進めます。詳細はそれぞれのページをご覧ください。

## 手順

1. [担当カットの決定](./assign.md)
2. 作画打ち合わせ
3. 作画作業
4. [アップロード](./upload.md)
5. 監督チェック


## 作画資料などの場所

設定資料は以下にあります。

<https://gitlab.com/kosys/kosys-common/>

設定資料のうち主な内容へのリンクです。

* [キャラデザ](https://gitlab.com/kosys/kosys-common/-/tree/master/designs/character)
* [制服](https://gitlab.com/kosys/kosys-common/-/tree/master/designs/uniform)
* [その他デザイン](https://gitlab.com/kosys/kosys-common/-/tree/master/designs)

また、現時点ではまだアップロードしていませんが、[設定資料集](../../docs/README.md)にも掲載予定です。

