# 撮影作業の流れ

1. [担当カットの決定](./assign.md)担当カットの決定
2. [レイヤー整理](./normalize-layers.md)
3. [仮撮影](./composite-kari.md)
4. [本撮影](./composite-hon.md)
5. [撮影修正方法](./fix-problem.md)
