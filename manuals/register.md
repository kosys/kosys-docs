# 参加登録

「こうしす！」制作プロジェクトにご参加いただくにあたり、制作団体であるOpen Process Animation Project Japan (OPAP-JP)に一般会員としてユーザーアカウントを登録していただく必要があります。

## 主な規約等

まず、当団体の規約をご一読ください。

* [定款](https://opap.jp/about/articles)
* [プライバシーポリシー](https://opap.jp/legal/privacy)


## アカウント登録

2つのサイトでアカウント登録をしていただく必要があります。
GitLabとJIRAです。

GitLabは成果物の管理に使用しているシステムで、JIRAは進捗管理を行うために使用しているシステムです。以下に登録方法を説明します。

以下の操作方法は2020年12月現在のものです。現在では登録方法が異なる場合があります。もし分からないことがあればTwitter等でお問い合わせください。


### GitLabに登録する

#### 1. ユーザー登録ページでユーザー登録する

<https://gitlab.com/users/sign_in>

![](./images/gitlab_001.png)

ユーザー情報を入力して登録する他に、GoogleアカウントやTwitterアカウントでログインすることも可能です。

ここではユーザー情報を入力して登録する方法について説明します。


* **Full Name**には、表示名を入力して下さい。（ペンネーム、ハンドルネームでＯＫです）
* **User Name**には、ログイン時に使用するユーザIDを指定して下さい。
* **Email**と**Email confirmation**には、同じEメールアドレスを入力して下さい。
* **Password**には、8文字以上のパスワードを入力して下さい。

上記の入力を終えたら、「I accept the Terms of Service and Privacy Policy」（利用規約とプライバシーポリシーに同意する）にチェックをいれて、**Register**（登録）ボタンを押してください。

::: warning
ここで同意を求められている利用規約とプライバシーポリシーはOPAP-JPのものではなく、GitLabのものです。
:::

上記で登録したメールアドレスにGitLabから「Confirmation instructions」というメールアドレス確認メールが届きます。
「Confirm your email address」（メールアドレスを確認する）のリンクをクリックして下さい。

（※メールアドレス確認メールはTwitter等でログインされた方には届きません。次のプロフィール設定でメールアドレスを登録することで届きますので、確認を行って下さい）

#### 2.プロフィールを設定する

以下のURLからプロフィールを設定して下さい。

<https://gitlab.com/profile>

![](./images/gitlab_002.png)
![](./images/gitlab_003.png)

1. 名前を必要に応じて編集します。  
（※Twitterログインされた場合は、Twitterに登録された名前が転記されています。なお、Twitterの名前を変更しても反映されませんのご留意ください）
2. メールアドレス欄が空欄ならメールアドレスを記入して下さい。（※例えば、Twitterログインを用いた場合は空欄になっています。）
    *  **Public Email**（公開するメールアドレス）に**Do not show on profile**（プロフィールに表示しない）を選択します。
    *  **Commit Email**（投稿者として公開されるメールアドレス）に**Use a private email**（秘匿用のメールアドレスを使用する）を選択します。  
（※本当のメールアドレスの代わりに「ユーザーID@users.noreply.gitlab.com」という形式のメールアドレスが使用されます）
3. 必要に応じてチェックを入れます。よく分からなければそのままでOKです。
4. **Update profile settings** ボタンを押して、設定を保存します。

※ここで、Eメールアドレスを初めて登録した場合、あるいは、メールアドレスを変更した場合、登録したメールアドレスにGitLabから「Confirmation instructions」というメールアドレス確認メールが届きます。「Confirm your email address」（メールアドレスを確認する）のリンクをクリックして下さい。

#### 3. 表示を日本語化する

以下から、**Preferences**のページを開いて下さい。（上記でメールアドレスを設定した後に開けるようになります）

<https://gitlab.com/profile/preferences>

プロフィール設定画面からの場合、左のメニューの**Preferences**からも開くことができます

![](./images/gitlab_004.png)
画面が開いたら、下の方にスクロールにします。


![](./images/gitlab_005.png)

**Localization**の項目を「English」から「日本語」に変更して、**Save changes**（変更を保存）ボタンで設定を保存して下さい。
これで、画面が日本語化されるはずです。（一部、英語のまま残る部分もあります）

#### 4. 登録したユーザー名を連絡する

OPAP-JP Twitter [@opap_jp](https://twitter.com/opap_jp)または[その他のお問い合わせ先](https://opap.jp/contact/)までDMでご連絡ください。プロジェクトに招待します。



### JIRAに登録する


#### 1. OPAP-JP JIRAを開きます

以下のURLにアクセスします
<https://opap.atlassian.net/>



#### 2. ログインアイコンを押します
![](./images/register_001.png)


#### 3. 開いたログイン画面で、「アカウントにサインアップする」をクリックします


![](./images/register_002.png)

※既に他の組織でJira Cloudを使用している場合は、ここで、そのアカウントでログインすることも可能です。しかし、それを望まれない場合は、そのアカウントとは異なるメールアドレスで登録をお願いします。

#### 4. 登録情報を入力して「サインアップ」ボタンを押します


![](./images/register_003.png)


* メールアドレス
    * あなたのメールアドレスを入力してください。更新通知が配信されたり、各種連絡のために使用されます。
* 氏名
    * 表示する名前を入力します。
    * **この内容は公開されます。** 本名の記載は推奨しません。
* パスワード
    * ログインの時に使用するパスワードを入力して下さい。
    * **他のサイトのパスワードを使い回さないでください**

::: warning
ここで同意を求められている利用規約とプライバシーポリシーはOPAP-JPのものではなく、アトラシアン社のものです。
:::

#### 5. 「atlassian.com」から確認メールが送付されます

「メールアドレスを認証」リンクから認証を完了して下さい。

::: warning
偽メールの可能性を考えると、そのままクリックするはあまりお勧めできません。右クリックでリンクアドレスをコピーし、アドレスバーに貼り付け、URLが「https://id.atlassian.com/」から始まることを確認することをお勧めします。

![](./images/register_004.png)

:::

もし数時間待ってもメールが届かない場合は、迷惑メールフォルダに入っている可能性があります。迷惑メールにもない場合はメールアドレスを間違えた可能性がありますので、再度最初からやりなおしてください。

#### 6. ログイン画面が表示されますので、4で登録したパスワードを入力して下さい

![](./images/register_005.png)


#### 7. 「Join on opap」ボタンをクリックします
![](./images/register_006.png)

#### 8. ホーム画面に戻りますので、もう一度サインインボタンをクリックします
![](./images/register_007.png)


#### 9. 以下の画面が表示されたら「スキップ」をクリックします
![](./images/register_008.png)

#### 10. ログイン状態でホーム画面が表示されたら終了です
![](./images/register_009.png)

※ログイン状態かどうかは右上がユーザーアイコン（初期状態では人のシルエットのアイコン）になっているかどうかで判断できます。

